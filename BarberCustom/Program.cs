﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BarberCustom
{
    static class Config
    {
        public const bool simulateTime = false;
        public const bool barberThread = true;
        public const bool customerThread = true;
    }

    class Program
    {
        // TODO: implement Mutex

        static void Main(string[] args)
        {
            Console.WriteLine("THE SLEEPING BARBER.\n");

            List<Customer> customers = Helper.GetCustomerList();
            Customer lastCustomer = null;

            customers.ForEach(delegate(Customer customer)
            {
                Helper.SimulateTimeRandom(0, 8);

                if (Config.customerThread)
                {
                    Thread customerThread = new Thread(customer.EnterBarberShop);
                    customerThread.Start();
                } else
                {
                    customer.EnterBarberShop();
                }

                lastCustomer = customer;
            });

            Console.WriteLine("\n{0} is last customer!\n", lastCustomer.name);
        }
    }

    static class Barber
    {
        public static void Wake(Customer customer)
        {
            Barber.ServiceCustomer(customer);
            Barber.CheckWaitingRoom();
            Barber.GoToSleep();
        }

        private static void ServiceCustomer(Customer customer)
        {
            BarberChair.PlaceCustomer(customer);
            Console.WriteLine("Barber{1}: Servicing {0}.", customer.name, Helper.threadId);
            Helper.SimulateTime(4);
            Console.WriteLine("Barber{1}: Finished servicing {0}.", customer.name, Helper.threadId);
            BarberChair.FreeCustomer();
            customer.LeaveBarberShop();
        }

        private static void CheckWaitingRoom()
        {
            Console.WriteLine("Barber{0}: Checking waiting room.", Helper.threadId);
            Helper.SimulateTime(1);

            Customer customer = WaitingRoom.DequeueCustomer();

            if (customer == null)
            {
                Console.WriteLine("Barber{0}: Waiting room is empty.", Helper.threadId);
                return;
            }

            Barber.ServiceCustomer(customer);
            Barber.CheckWaitingRoom();
        }

        private static void GoToSleep()
        {
            Console.WriteLine("Barber{0}: Going to sleep.", Helper.threadId);

            // TODO: Release Mutex
        }
    }

    static class BarberChair
    {
        static private Customer customer = null;

        public static bool PlaceCustomer(Customer customer)
        {
            if (BarberChair.IsFree())
            {
                BarberChair.customer = customer;
                return true;
            }

            return false;
        }

        public static Customer FreeCustomer()
        {
            if (BarberChair.IsFree())
            {
                return null;
            }

            Customer customer = BarberChair.customer;
            BarberChair.customer = null;

            return customer;
        }

        public static bool IsFree()
        {
            if (BarberChair.customer == null)
            {
                return true;
            }

            return false;
        }
    }

    static class WaitingRoom
    {
        private const int chairs = 2;
        private static Queue<Customer> customers = new Queue<Customer>();

        public static bool EnqueueCustomer(Customer customer)
        {
            if (WaitingRoom.IsFull())
            {
                return false;
            }

            WaitingRoom.customers.Enqueue(customer);
            return true;

        }

        public static Customer DequeueCustomer()
        {
            if (WaitingRoom.IsEmpty())
            {
                return null;
            }

            return WaitingRoom.customers.Dequeue();
        }

        public static bool IsEmpty()
        {
            if (WaitingRoom.customers.Count <= 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsFull()
        {
            if (WaitingRoom.customers.Count >= WaitingRoom.chairs)
            {
                return true;
            }

            return false;
        }
    }

    class Customer
    {
        public string name { get; private set; }

        public Customer(string name = "Bob")
        {
            this.name = name;
        }

        public void EnterBarberShop()
        {
            Console.WriteLine("{0}: Entering barber shop.", this.name);
            Helper.SimulateTime(1);

            if (WaitingRoom.IsFull())
            {
                Console.WriteLine("{0}: Waiting room is full.", this.name);
                this.LeaveBarberShop();
                return;
            }

            if (WaitingRoom.IsEmpty())
            {
                if (BarberChair.IsFree())
                {
                    Console.WriteLine("{0}: Waking barber.", this.name);
                    Helper.SimulateTime(2);

                    if (Config.barberThread)
                    {
                        Thread barberThread = new Thread(() => Barber.Wake(this));
                        barberThread.Start();
                    } else
                    {
                        Barber.Wake(this);
                    }

                    return;
                }
            }

            Console.WriteLine("{0}: Sits in the waiting room.", this.name);
            WaitingRoom.EnqueueCustomer(this);
        }

        public void LeaveBarberShop()
        {
            Helper.SimulateTime(1);
            Console.WriteLine("{0}: Leaving barber shop.", this.name);
        }
    }

    static class Helper
    {
        public static Random random = new Random();
        public static int threadId { get{ return Thread.CurrentThread.ManagedThreadId; } }

        public static List<Customer> GetCustomerList()
        {
            List<Customer> customers = new List<Customer>();
            customers.Add(new Customer("James"));
            customers.Add(new Customer("Mary"));
            customers.Add(new Customer("John"));
            customers.Add(new Customer("Patricia"));
            customers.Add(new Customer("Robert"));
            customers.Add(new Customer("Jennifer"));
            customers.Add(new Customer("Michael"));
            customers.Add(new Customer("Linda"));
            customers.Add(new Customer("William"));
            customers.Add(new Customer("Elizabeth"));
            customers.Add(new Customer("David"));
            customers.Add(new Customer("Barbara"));
            customers.Add(new Customer("Richard"));
            customers.Add(new Customer("Susan"));
            customers.Add(new Customer("Joseph"));
            customers.Add(new Customer("Jessica"));
            customers.Add(new Customer("Thomas"));
            customers.Add(new Customer("Sarah"));
            customers.Add(new Customer("Charles"));
            customers.Add(new Customer("Margaret"));
            customers.Add(new Customer("Christopher"));
            customers.Add(new Customer("Karen"));
            customers.Add(new Customer("Daniel"));
            customers.Add(new Customer("Nancy"));
            customers.Add(new Customer("Matthew"));
            customers.Add(new Customer("Lisa"));

            return customers;
        }

        public static void SimulateTime(int seconds)
        {
            if (Config.simulateTime) { Thread.Sleep(1000 * seconds); }
        }

        public static void SimulateTimeRandom(int secondsMin, int secondsMax)
        {
            if (Config.simulateTime) { Thread.Sleep(Helper.random.Next(secondsMin * 1000, secondsMax * 1000)); }
        }
    }
}
